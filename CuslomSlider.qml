import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4

Rectangle
{
    id: slrc
    width: 200
    height:sl.height + txtRec.height + 20
    color: "transparent"
    property bool isK1: true
    property real koeff
    property alias value: sl.value
    property string name

    Slider
  {
      id:sl
      width: parent.width
      stepSize: 1
      from:0
      to: 1024
      value:0
      onValueChanged: {
      if (isK1) koeff = 1+value/1024*100;
      else koeff = (value/1024)*(20/10)*5;
      }

  }

        Label
        {
            anchors.right: txtRec.left
            anchors.bottom: txtRec.bottom
            anchors.margins: 5
            text: name
        }
        Rectangle{
        id: txtRec
        width: 44
        height: 22
        anchors.top: sl.bottom
        anchors.horizontalCenter: sl.horizontalCenter
        color: Qt.darker("darkgray")
        Text
        {
            anchors.fill: parent
            anchors.margins: 2
            id: txt
            color: "white"
            text: qsTr(koeff.toString())
            font.pointSize: 12
        }
    }

}
