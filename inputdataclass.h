#ifndef INPUTDATACLASS_H
#define INPUTDATACLASS_H
#include <QObject>
#include <QVector>
#include <QMap>
class InputDataClass: public QObject
{
    Q_OBJECT
public:
    InputDataClass(QObject *parent = 0);
    Q_INVOKABLE void setData(void);
    void getData(uint16_t &index, uint16_t &data);
signals:
    void dataChanged1(int x, int y);
    void dataChanged2(int x, int y);
private:
  int arraySize = 5000;
  QMap<int16_t,int16_t>dataMap1;
  QMap<int16_t,int16_t>dataMap2;
//  int16_t dataArray1[5000];
//  int16_t dataArray2[5000];
};

#endif // INPUTDATACLASS_H
