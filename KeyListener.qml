import QtQuick 2.9
import QtQuick.Controls 2.2

Item
{
 id: keyListener
 anchors.fill: parent
 focus: true
 property int screenNum:1
 property alias  loadSourse: loader.source

 Keys.onPressed:
 {
  console.log (event.key)
  switch (event.key)
  {
  case Qt.Key_1: loader.source = "qrc:/Screen1.qml"
       screenNum = event.key - 48
  break;
  case Qt.Key_2: loader.source = "qrc:/Screen2.qml"
      screenNum = event.key - 48
  break;
  case Qt.Key_3: loader.source = "qrc:/Screen3.qml"
      screenNum = event.key - 48
  break;

  default: loader.source = "qrc:/Screen1.qml"      //временно
      screenNum = event.key - 48
 }
}
 Loader {
     id: loader
     anchors.fill: parent
     source: "qrc:/Screen1.qml"
 }
}
