import QtQuick 2.9
import QtCharts 2.2

Rectangle{
    anchors.fill: parent

ChartView {
   title : "Line"
   anchors .fill: parent
   antialiasing : true
   ValueAxis
   {
    id:xAxis
    min:0
    max:5000
   }
   ValueAxis
   {
    id:yAxis
    min:0
    max:5000
   }

   LineSeries {
       id:ls
       name : "LineSeries"
       pointsVisible: true
       axisX: xAxis
       axisY: yAxis
   }
   Connections{
       target: dArray
       onDataChanged2: { ls.append(x,y)}

   }
   Component.onCompleted: dArray.setData()
}
}
