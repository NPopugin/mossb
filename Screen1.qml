import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
Rectangle{
    id: mRec
    property int marg: 5
    property real koeff3:0
    property real tVal:0.0001
    property real tiVal:0.01
    property real tdVal:0.0004
    anchors.fill: parent

    CuslomSlider{id:sl1; anchors.top: mRec.top; anchors.margins:marg; isK1: true; name: "K1:"}
    CuslomSlider{id:sl2; anchors.top: sl1.bottom; anchors.margins:marg; isK1: false; name: "K2:" }
    CuslomSlider{id:sl3; anchors.top: sl2.bottom; anchors.margins:marg; isK1: false; name: "K:" }

    Rectangle{
    id: txtRec
    width: 60
    height: 22
    anchors.top: sl3.bottom
    anchors.horizontalCenter: sl2.horizontalCenter
    color: Qt.darker("darkgray")

    TextInput {
    id: k3
    anchors.fill: parent
    anchors.margins: 2
    font.pointSize: 12
    text: "1"
    onEditingFinished: koeff3 = text
    color: "white"
    }
    }
    Label
    {
        anchors.right: txtRec.left
        anchors.bottom: txtRec.bottom
        anchors.margins: 5
        text: "K3:"
    }
    Rectangle{
    id: koefRec
    width: 60
    height: 22
    anchors.top: txtRec.bottom
    anchors.horizontalCenter: txtRec.horizontalCenter
    anchors.margins: marg
    color: Qt.darker("darkgray")

    Text {
        id: k
        font.pointSize: 12
        text: qsTr((koeff3*sl1.koeff*sl2.koeff).toString())
        color: "white"
    }
    }
    Label
    {
        anchors.right: koefRec.left
        anchors.bottom: koefRec.bottom
        anchors.margins: 5
        text: "Крег:"
    }
    Rectangle{
    id: tRec
    width: 60
    height: 22
    anchors.left: parent.horizontalCenter
    anchors.top: parent.top
    anchors.margins: marg
    color: Qt.darker("darkgray")

    TextInput {
    id: t
    anchors.fill: parent
    anchors.margins: 2
    font.pointSize: 12
    text: tVal.toString()
    onEditingFinished: tVal = text
    color: "white"
    }
    }
    Label
    {
        anchors.right: tRec.left
        anchors.bottom: tRec.bottom
        anchors.margins: 5
        text: "T:"
    }
    Rectangle{
    id: tiRec
    width: 60
    height: 22
    anchors.top: tRec.bottom
    anchors.horizontalCenter: tRec.horizontalCenter
    anchors.margins: marg
    color: Qt.darker("darkgray")

    TextInput {
    id: ti
    anchors.fill: parent
    anchors.margins: 2
    font.pointSize: 12
    text: tiVal.toString()
    onEditingFinished: tiVal = text
    color: "white"
    }
    }
    Label
    {
        anchors.right: tiRec.left
        anchors.bottom: tiRec.bottom
        anchors.margins: 5
        text: "Tинт:"
    }
    Rectangle{
    id: tdRec
    width: 60
    height: 22
    anchors.top: tiRec.bottom
    anchors.horizontalCenter: tiRec.horizontalCenter
    anchors.margins: marg
    color: Qt.darker("darkgray")


    TextInput {
    id: td
    anchors.fill: parent
    anchors.margins: 2
    font.pointSize: 12
    text: tdVal.toString()
    onEditingFinished: tdVal = text
    color: "white"
    }
    }
    Label
    {
        anchors.right: tdRec.left
        anchors.bottom: tdRec.bottom
        anchors.margins: 5
        text: "Tдиф:"
    }
    Button
    {
     id: setkoeffButton
     anchors.top: tdRec.bottom
     anchors.horizontalCenter: tiRec.horizontalCenter
     anchors.margins: marg
     text: "Применить"
     onClicked: cpp.getKoeffValues(sl1.value, sl2.value, sl3.value, sl1.koeff, sl2.koeff, koeff3, tVal, tiVal, tdVal)
    }

    Button
    {
     id: sendFileButton
     anchors.top: setkoeffButton.bottom
     anchors.horizontalCenter: tiRec.horizontalCenter
     anchors.margins: marg
     text: "Файл"
     onClicked: cpp.sendFile()
    }


}
