import QtQuick.Window 2.3
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQml.Models 2.3
import QtQuick 2.9
import QtCharts 2.2

ApplicationWindow {
    property int marg: 5
    property color textColor: "black"
    property color borderColor: "#C6C6C6"
    property int textSize: 13
    id: appWindow
    visible: true
    minimumWidth:800
    minimumHeight:600
    title: qsTr("Mossb")

    KeyListener{id: keyListener}
header: ToolBar
{
    RowLayout {

    ToolButton
    {
     text: "Подключение"
     onClicked: cpp.openSerialPort()
    }
    ToolButton
    {
     text: "Отключение"
     onClicked: cpp.closeSerialPort()
    }
    ToolButton
    {
     text: "Настройки"
     onClicked: cpp.showSettings()
    }
    ToolButton
    {
     text: "Обновить"
     onClicked: cpp.sendRequest()
    }
    }

}
footer:
    TabBar
    {
     id: tabBar
     width: parent.width
     currentIndex: keyListener.screenNum - 1
     Repeater
     {
         model: [1, 2, 3]
         TabButton
         {
           text: modelData
           onClicked: {
                      keyListener.loadSourse = "qrc:/screens/Screen"+modelData+".qml"
                      keyListener.focus = true
                      }
         }
     }
    }
 ToolBar
{
    height: lb.height+5

 Label
 {
     id: lb
     anchors.left: parent.left
   anchors.margins: marg
   Connections
   {
    target: cpp
    onStatusMessageChanged:{
    lb.text =  message
    }
   }

 }
}
}//ApplicationWindow
