import QtQuick 2.9
import QtCharts 2.2
import QtQuick.Controls 2.3

Rectangle{
    property string pointVal
    anchors.fill: parent
        Label
        {
            id: lb
            anchors.left: parent.left
            anchors.bottom: parent.bottom
          anchors.margins: marg
          text: pointVal
        }
ChartView {
   title : "Line"
   //anchors.fill: parent
   anchors.left: parent.left
   anchors.right: parent.right
   anchors.top: parent.top
   anchors.bottom: lb.top
   antialiasing : true
   ValueAxis
   {
    id:xAxis
    min:0
    max:5000
   }
   ValueAxis
   {
    id:yAxis
    min:0
    max:17000
   }

   LineSeries {
       id:ls
       name : "LineSeries"
       pointsVisible: true
       axisX: xAxis
       axisY: yAxis
       onHovered: {
       if(state) pointVal = point.x + ", " + point.y
       else pointVal = " "
       }
   }
   Connections{
       target: dArray
       onDataChanged1: { ls.append(x,y)}

   }
   Component.onCompleted: dArray.setData()
}
}
