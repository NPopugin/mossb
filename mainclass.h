#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QFile>
#include <QFileDialog>
#include <QDebug>
#include "settingsdialog.h"
#include "inputdataclass.h"

class MainClass: public QObject
{
    Q_OBJECT

public:
    MainClass(QObject *parent = 0);
    ~MainClass();
    Q_INVOKABLE  void openSerialPort();
    Q_INVOKABLE void showSettings();
    Q_INVOKABLE void closeSerialPort();
    Q_INVOKABLE void getKoeffValues(int D1, int D2, int D3, double K1, double K2, double K3, double T, double Ti, double Td);
    Q_INVOKABLE void sendFile();
    Q_INVOKABLE void sendRequest();
private slots:


    void writeData(const QByteArray &data);
    void readData();
    void handleError(QSerialPort::SerialPortError error);
signals:
    void statusMessageChanged(const QString &message);
private:
    void initActionsConnections();
    void showStatusMessage(const QString &message);

    typedef struct
    {
      double a1;
      double a2;
      double b1;
      double b2;
      uint16_t D1;
      uint16_t D2;
      uint16_t D3;
    }koeffStr;
    void sendKoeffValues(koeffStr &kstr);
    SettingsDialog *settings = nullptr;
    QSerialPort *serial;
    QQmlApplicationEngine *engine;
    InputDataClass *dataArray;
};

#endif // MAINCLASS_H
