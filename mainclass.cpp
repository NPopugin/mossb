#include "mainclass.h"
#include <QMessageBox>
#include <QDesktopWidget>

MainClass::MainClass(QObject *parent) : QObject(parent)
{
    engine = new QQmlApplicationEngine;

    QQmlContext *rootContext = engine->rootContext();
    rootContext->setContextProperty("cpp", this);

    serial = new QSerialPort(this);

    settings = new SettingsDialog;

    dataArray = new InputDataClass();
    rootContext->setContextProperty("dArray", dataArray);




    initActionsConnections();
    engine->load(QUrl(QStringLiteral("qrc:/main.qml")));

}

MainClass::~MainClass()
{
    delete settings;
}



void MainClass::openSerialPort()
{
    SettingsDialog::Settings p = settings->settings();
    serial->setPortName(p.name);
    serial->setBaudRate(p.baudRate);
    serial->setDataBits(p.dataBits);
    serial->setParity(p.parity);
    serial->setStopBits(p.stopBits);
    serial->setFlowControl(p.flowControl);
    if (serial->open(QIODevice::ReadWrite)) {
        showStatusMessage(tr("Подключено к %1 : %2, %3, %4, %5, %6")
                          .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                          .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));

     }
 else {
        QMessageBox::critical(NULL, tr("Ошибка"), serial->errorString());

        showStatusMessage(tr("Ошибка подключения"));
    }
}

void MainClass::closeSerialPort()
{
    if (serial->isOpen())
        serial->close();
    showStatusMessage(tr("Отключено"));
}

void MainClass::writeData(const QByteArray &data)
{
    serial->write(data);
}

void MainClass::readData()
{
//  QByteArray data = serial->readAll();
//  uint16_t idata = data[0];
//  qDebug()<<"Input:"<<idata;
static QByteArray data;
data.append(serial->readAll());
qDebug()<<"Input:"<<data;
//----------------
if (data.size()>128) data.clear();
while (!data.isEmpty())
{
    int i = data.indexOf("b");
    if (i == -1)
    {
        data.clear();
        break;
    }
    data.remove(0,i+1);
    i = data.indexOf("s");
    if (i<=4 && i!=-1)
    {
      uint8_t index1 = data[0];
      uint8_t index2 = data[1];
      uint16_t indexout = (index1<<8) + index2;
      uint8_t inpdta1 = data[2];
      uint8_t inpdta2 = data[3];
      uint16_t dataout = (inpdta1<<8) + inpdta2;
      dataArray->getData(indexout, dataout);
      qDebug()<<"Index:"<<indexout<<"Data1:"<<inpdta1<<"Data2:"<<dataout;
    }
    else break;
 }
//----------------


//if (data.size()>128) data.clear();
//while (!data.isEmpty())
//{
//    int i = data.indexOf("b");
//    if (i == -1)
//    {
//        data.clear();
//        break;
//    }
//    data.remove(0,i);
//    i = data.indexOf("s");
//    if (i<=7 && i!=-1)
//    {
//        dataArray->getData(data.left(i));
//        data.remove(0,i);
//    }
//    else break;
// }
}

void MainClass::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(NULL, tr("Критическая ошибка"), serial->errorString());
        closeSerialPort();
    }
}

void MainClass::initActionsConnections()
{

    connect(serial, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
            this, &MainClass::handleError);
    connect(serial, &QSerialPort::readyRead, this, &MainClass::readData);
}

void MainClass::showStatusMessage(const QString &message)
{
   emit statusMessageChanged(message);
}



void MainClass::showSettings()
{
    settings->show();
}

void MainClass::sendFile()
{
    qDebug()<<"startsend";
  QFile file(QFileDialog::getOpenFileName());
  if (!file.open(QIODevice::ReadOnly))
  { qDebug()<<"error";
      return;
}
  while (!file.atEnd()) {
       QByteArray line = file.readLine();
       for (int i = 0; i<line.size(); i++)
       {
           QByteArray Data;
           Data[0]=line.at(i);
           if(Data[0]!=0x00) serial->write(Data);
           // qDebug()<<Data;
       }
//     for (int i = 0; i<10; i++){
//               QByteArray Data;
//               Data[0]=58;
//               Data[1]=59;
//     serial->write(Data);
//      }
   }


  showStatusMessage(tr("Sended"));

}
void MainClass::getKoeffValues(int D1, int D2, int D3, double K1, double K2, double K3, double T, double Ti, double Td)
{
 qDebug()<<D1<<D2<<K1<<K2<<K3<<T<<Ti<<Td;
 koeffStr k;
 k.a1 = (2*Td+T)*K3;
 k.a2 = (2*Td-T)*K3;
 qDebug()<<k.a1<<k.a2;
 k.b1 = 2*Ti + T;
 k.b2 = T - 2*Ti;
 k.D1 = D1;
 k.D2 = D2;
 k.D3 = D3;
 sendKoeffValues(k);
}
void MainClass::sendKoeffValues(koeffStr &kstr)
{
    QByteArray data;                              //0x11-0x16: a1,a2,b1,b2,d1,d2
//    data[0]=0x11;
//    int16_t temp = static_cast<int16_t>(kstr.a1*10000);
//    data[1]= temp>>8;
//    data[2]= temp;
//    writeData(data);
//    qDebug()<<"write"<<data;

//    data[0]=0x12;
//    temp = static_cast<int16_t>(kstr.a2*10000);
//    data[1]= temp>>8;
//    data[2]= temp;
//    writeData(data);
//    qDebug()<<"write"<<data;

//    data[0]=0x13;
//    temp = static_cast<int16_t>(kstr.b1*10000);
//    data[1]= temp>>8;
//    data[2]= temp;
//    writeData(data);
//qDebug()<<"write"<<data;

//    data[0]=0x14;
//    temp = static_cast<int16_t>(kstr.b2*10000);
//    data[1]= temp>>8;
//    data[2]= temp;
//    writeData(data);
//qDebug()<<"write"<<data;


    int16_t temp = static_cast<int16_t>(kstr.D1);
    data[0]= temp>>8;
    data[1]= temp;
    data[2]=0x15;
 //   writeData(data);
//qDebug()<<"write"<<data;


    temp = static_cast<int16_t>(kstr.D2);
    data[3]= temp>>8;
    data[4]= temp;
    data[5]=0x16;
//    writeData(data);
//qDebug()<<"write"<<data;


temp = static_cast<int16_t>(kstr.D3);
data[6]= temp>>8;
data[7]= temp;
data[8]=0x17;

temp = static_cast<int16_t>(kstr.a1*10000);
data[9]= temp>>8;
data[10]= temp;
data[11]=0x18;

temp = static_cast<int16_t>(kstr.a2*10000);
data[12]= temp>>8;
data[13]= temp;
data[14]=0x19;
writeData(data);

qDebug()<<"write"<<data;
}

void MainClass::sendRequest()
{
    QByteArray data;
    data[0]='r';
    qDebug()<<data;
    writeData(data);
}
